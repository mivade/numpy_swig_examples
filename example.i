%module(docstring="various numpy/swig examples") example

%feature("autodoc", "1");

%{
    #define SWIG_FILE_WITH_INIT
    #include "example.h"
%}

%include "numpy.i"
%include "stl.i"

%init %{
    import_array();
%}

%template(vectori) std::vector<int>;

%apply (double *IN_ARRAY1, int DIM1) {(double *a, int len1), (double *b, int len2)};

%include "example.h"

%rename (dot) mydot;
%inline %{
    double mydot(double *a, int len1, double *b, int len2) {
        return dot(a, b, len1);
    }
%}
