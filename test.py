from timeit import timeit
import numpy as np
import example

LIM = 1000


def test_dot():
    a = np.random.random(100)
    b = np.random.random(100)
    print(example.dot(a, b))


def test_irange_asarray(lim=LIM):
    irange = np.asarray(example.irange(0, lim))
    return irange


def test_irange(lim=LIM):
    irange = example.irange(0, lim)
    return irange


if __name__ == "__main__":
    from functools import partial

    timeit = partial(timeit, number=100, globals=globals())

    print("irange, no wrap", timeit('test_irange()'))
    print("irange, wrap", timeit('test_irange_asarray()'))
