from setuptools import setup, Extension
import numpy as np

modules = [
    Extension(
        '_example',
        sources=['example.i', 'example.cpp'],
        include_dirs=[np.get_include()],
        swig_opts=['-c++'],
        extra_compile_args=['-std=c++14'],
    ),
]

setup(
    name="example",
    ext_modules=modules,
)
