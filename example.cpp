#include <algorithm>

#include "example.h"


double dot(double *a, double *b, int len)
{
    double result = 0;

    for (int i = 0; i < len; ++i) {
        result += a[i] * b[i];
    }

    return result;
}


std::vector<int> irange(int a, int b)
{
    std::vector<int> result(b - a);
    int n = 0;
    std::generate(result.begin(), result.end(), [&n]() { return n++; });
    return result;
}
