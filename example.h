#ifndef EXAMPLE_H
#define EXAMPLE_H

#include <vector>

/// Compute the dot product of two vectors of the same length
double dot(double *a, double *b, int len);

/// Generate a list of integers on the interval [a, b)
std::vector<int> irange(int a, int b);

#endif
